﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using MM.Monitor.Client;
using Version = MM.Monitor.Client.Version;

namespace NotificationBridgePlugin
{
    public class NotificationBridgePlugin : ClientPlugin, IDisposable
    {
        internal static NotificationBridgePlugin Instance { get; private set; }
        private bool disposed;
        private ServiceHost service;

        public NotificationBridgePlugin()
        {
            Instance = this;
        }

        public override string GetPluginName()
        {
            return "Notification Bridge";
        }
        public override string GetPluginDescription()
        {
            return "A bridge between Pulseway notification system and scripts.";
        }
        public override Version GetPluginVersion()
        {
            FileVersionInfo ver = FileVersionInfo.GetVersionInfo(typeof (NotificationBridgePlugin).Assembly.Location);
            return new Version(ver.FileMajorPart, ver.FileMinorPart);
        }
        public override void PluginLoaded()
        {
            StartService();
        }
        public override void PluginUnloaded()
        {
            if (!disposed)
            {
                StopService();
            }
        }

        public void Dispose()
        {
            if (!disposed)
            {
                StopService();
            }
        }

        private void StartService()
        {
            service = new ServiceHost(typeof(NotificationBridgeService));
            try
            {
                service.AddServiceEndpoint(typeof(INotificationBridgeService), new NetNamedPipeBinding(), "net.pipe://localhost/NotificationBridge");
                service.Open();
                Trace("Service is listening on: net.pipe://localhost/NotificationBridge...");
            }
            catch (Exception ex)
            {
                Trace("Service failed to start. Error was: " + ex.Message);
            }
        }
        private void StopService()
        {
            if (disposed)
                return;
            disposed = true;

            if (service != null && service.State == CommunicationState.Opened)
            {
                service.Abort();
            }
        }
    }
}