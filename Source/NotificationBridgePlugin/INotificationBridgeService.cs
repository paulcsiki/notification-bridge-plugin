﻿using System.ServiceModel;

namespace NotificationBridgePlugin
{
    [ServiceContract]
    public interface INotificationBridgeService
    {
        [OperationContract]
        bool SendNotificationToAllDevices(string message, int priority, bool allowRepeating);
    }
}