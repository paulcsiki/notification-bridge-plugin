﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Notification Bridge")]
[assembly: AssemblyDescription("A bridge between the Pulseway notification system and scripts.")]
[assembly: AssemblyCompany("Alexandru Paul Csiki")]
[assembly: AssemblyProduct("Notification Bridge")]
[assembly: AssemblyCopyright("Copyright © Alexandru Paul Csiki 2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("c82b6e0e-edba-4628-9cfa-1a45747803d8")]
[assembly: AssemblyVersion("1.3.*")]
[assembly: AssemblyFileVersion("1.3.0.0")]