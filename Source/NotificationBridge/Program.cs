﻿using System;
using System.IO;
using System.ServiceModel;
using NotificationBridgePlugin;

namespace NotificationBridge
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("=Notifications Bridge 1.1=");
                Console.WriteLine();
                Console.WriteLine("Usage:");
                Console.WriteLine("NotificationsBridge.exe -p 1 -t \"message\"");
                Console.WriteLine("NotificationsBridge.exe -p 2 -f \"C:\\log.txt\"");
                Console.WriteLine();
                Console.WriteLine("Exit codes:");
                Console.WriteLine("0 - Notification successfully sent");
                Console.WriteLine("1 - Notification was not sent.");
                Environment.Exit(1);
            }

            try
            {
                Arguments arguments = Arguments.Parse(args);
                ChannelFactory<INotificationBridgeService> factory = new ChannelFactory<INotificationBridgeService>(new NetNamedPipeBinding(), "net.pipe://localhost/NotificationBridge");
                INotificationBridgeService channel = factory.CreateChannel();
                string message = GetMessage(arguments);
                if (string.IsNullOrWhiteSpace(message))
                    throw new Exception("Refusing to send an empty notification.");

                if (channel.SendNotificationToAllDevices(message, arguments.NotificationPriority, arguments.AllowRepeatingNotifications))
                {
                    Console.WriteLine("Notification was successfully sent.");
                    Environment.Exit(0);
                }
                else
                {
                    Console.WriteLine("Pulseway refused the notification.");
                    Environment.Exit(1);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Environment.Exit(1);
            }
        }

        private static string GetMessage(Arguments arguments)
        {
            try
            {
                return arguments.UseTextFile ? File.ReadAllText(arguments.TextFileName) : arguments.NotificationText;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}