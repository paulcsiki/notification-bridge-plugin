﻿using System.ServiceModel;
using MM.Monitor.Client;

namespace NotificationBridgePlugin
{
    [ServiceBehavior]
    public class NotificationBridgeService : INotificationBridgeService
    {
        public bool SendNotificationToAllDevices(string message, int priority, bool allowRepeating)
        {
            NotificationBridgePlugin plugin = NotificationBridgePlugin.Instance;
            ComputerInfo computerInfo = plugin.GetComputerInfo();

            if (!plugin.CanSendNotifications)
            {
                plugin.Trace("Tried to send a notification but Pulseway refused it.");
                return false;
            }
            if (computerInfo.InMaintenanceMode)
            {
                plugin.Trace("Tried to send a notification but Pulseway is in maintenance mode.");
                return false;
            }

            plugin.Trace("Received request to send notification: " + message);
            message += string.Format("\n\nSent from {0} in group {1}.", computerInfo.Name, computerInfo.Group);

            NotificationPriority notificationPriority =  NotificationPriority.NORMAL;
            switch (priority)
            {
                case 0:
                    notificationPriority = NotificationPriority.LOW;
                    break;
                case 1:
                    notificationPriority = NotificationPriority.NORMAL;
                    break;
                case 2:
                    notificationPriority = NotificationPriority.ELEVATED;
                    break;
                case 3:
                    notificationPriority = NotificationPriority.CRITICAL;
                    break;
            }

            return plugin.SendNotificationToAllDevices(message, notificationPriority, allowRepeating);
        }
    }
}