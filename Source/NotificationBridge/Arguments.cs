﻿using System;
using System.IO;

namespace NotificationBridge
{
    public class Arguments
    {
        public int NotificationPriority;
        public string NotificationText;
        public bool UseTextFile;
        public string TextFileName;
        public bool AllowRepeatingNotifications;

        public static Arguments Parse(string[] cliArguments)
        {
            if (cliArguments == null || cliArguments.Length == 0)
                throw new Exception("No arguments specified.");

            Arguments result = new Arguments();

            bool priorityFound = false, notificationTextFound = false, notificationFileFound = false;

            for (int i = 0; i < cliArguments.Length; i++)
            {
                string argument = cliArguments[i];
                if (string.IsNullOrWhiteSpace(argument))
                    continue;

                if (argument == "-p")
                {
                    EnsureArgumentValueIsPresent(cliArguments, argument, i + 1);
                    result.NotificationPriority = ParseNotificationPriority(cliArguments[++i]);
                    priorityFound = true;
                }
                else if (argument == "-t")
                {
                    EnsureArgumentValueIsPresent(cliArguments, argument, i + 1);
                    result.NotificationText = ParseNotificationText(cliArguments[++i]);
                    result.TextFileName = null;
                    result.UseTextFile = false;
                    notificationTextFound = true;
                }
                else if (argument == "-f")
                {
                    EnsureArgumentValueIsPresent(cliArguments, argument, i + 1);
                    result.NotificationText = null;
                    result.TextFileName = ParseTextFileName(cliArguments[++i]);
                    result.UseTextFile = true;
                    notificationFileFound = true;
                }
                else if (argument == "-r")
                {
                    result.AllowRepeatingNotifications = true;
                }
                else
                {
                    throw new Exception(string.Format("Unknown argument '{0}'.", argument));
                }
            }

            if (!priorityFound)
                throw new Exception("Notification priority was not specified.");
            if (!notificationTextFound && !notificationFileFound)
                throw new Exception("Both notification text and text file name arguments are missing! You must specify one of them.");
            if (notificationTextFound && notificationFileFound)
                throw new Exception("Both notification text and text file name arguments are specified! You only need to specify one of them.");

            return result;
        }

        private static int ParseNotificationPriority(string argument)
        {
            const string message = "'{0}' is not a valid notification priority (0=Low, 1=Normal, 2=Elevated, 3=Critical).";
            int intVal;

            if (!int.TryParse(argument, out intVal) || intVal < 0 || intVal > 3)
            {
                throw new Exception(message);
            }

            return intVal;
        }
        private static string ParseNotificationText(string argument)
        {
            if (string.IsNullOrWhiteSpace(argument))
                throw new Exception("Invalid notification text (empty value).");

            return argument;
        }
        private static string ParseTextFileName(string argument)
        {
            if (string.IsNullOrWhiteSpace(argument))
                throw new Exception("Invalid text file name (empty value).");

            if (!File.Exists(argument))
                throw new Exception("File name is invalid (missing file).");

            return argument;
        }
        private static void EnsureArgumentValueIsPresent(string[] arguments, string currentArgument, int index)
        {
            if (index >= arguments.Length)
                throw new Exception(string.Format("'{0}' has no value.", currentArgument));
        }
    }
}