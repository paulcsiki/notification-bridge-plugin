﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Notification Bridge Plugin")]
[assembly: AssemblyDescription("A bridge between Pulseway notification system and scripts.")]
[assembly: AssemblyCompany("Alexandru Paul Csiki")]
[assembly: AssemblyProduct("Notification Bridge")]
[assembly: AssemblyCopyright("Copyright © Alexandru Paul Csiki 2019")]
[assembly: ComVisible(false)]
[assembly: Guid("10ca5029-829a-469f-bc09-403bc760f3ca")]
[assembly: AssemblyVersion("1.3.*")]
[assembly: AssemblyFileVersion("1.3.0.0")]